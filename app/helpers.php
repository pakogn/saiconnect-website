<?php

use Illuminate\Support\Str;

function getTableData($connection, $sql)
{
    $results = $connection->Execute($sql);
    $payload = [];
    $columnsCount = $results->Fields->Count;

    if ($columnsCount > 0) {
        $columnIndex = 0;

        while (!$results->EOF) {
            for ($index = 0; $index < $columnsCount; $index++) {
                $payload[$columnIndex][$results->Fields($index)->Name] = trim(utf8_encode($results->Fields($index)));
            }

            $columnIndex++;

            $results->MoveNext();
        }
    }

    $results->Close();

    return $payload;
}

function dump_sql($builder)
{
    $sql = $builder->toSql();
    $bindings = $builder->getBindings();

    array_walk($bindings, function ($value) use (&$sql) {
        $value = is_string($value) ? var_export($value, true) : $value;
        $sql = preg_replace("/\?/", $value, $sql, 1);
    });

    return $sql;
}

function dump_command_sql($builder, $command, $bindings = [])
{
    $method = "compile{$command}";
    $sql = $builder->getGrammar()->{$method}($builder, $bindings);

    array_walk($bindings, function ($value) use (&$sql) {
        if (is_string($value)) {
            if (!empty($value) && $value[0] == '\\') {
                $value = Str::after($value, '\\');
            } else {
                $value = var_export($value, true);
            }
        }

        $sql = preg_replace("/\?/", $value, $sql, 1);
    });

    return $sql;
}

function dump_delete_sql($builder)
{
    $sql = $builder->getGrammar()->compileDelete($builder);

    return $sql;
}

function getNextId($conn, $idColumn, $table)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 {$idColumn} from {$table} order by {$idColumn} desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();
    return (string) $lastId;
}

function getNextConfigId($conn, $idColumn, $key)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 {$idColumn} from config where clave = '{$key}' order by clave desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function insertPedidoc($conn, $data)
{
    $record = array();
    $record['no_ped'] = $data['no_ped']; //Numeric
    $record['cve_cte'] = $data['cve_cte']; //Numeric
    $record['cve_age'] = $data['cve_age']; //Numeric
    $record['f_alta_ped'] = "CTOD('".date("m-d-Y")."')";
    $record['descuento'] = $data['descuento']; //Numeric
    $record['descue'] = $data['descue']; //Numeric
    $record['status'] = "'Por Surtir'"; //Character
    $record['total_ped'] = $data['total_ped']; //Numeric
    $record['impr_ped'] = 0; //Numeric
    $record['subt_ped'] = $data['subt_ped']; //Numeric

    $record['iva'] = $data['iva']; //Numeric
    $record['observa'] = $data['observa']; //Memo 4
    $record['cve_mon'] = 1; //Numeric
    $record['tip_cam'] = 2; //Numeric
    $record['cve_suc'] = $data['cve_suc']; //Character 3
    $record['mes'] = "CTOD('".date("m")."')";
    $record['año'] = "CTOD('".date("Y")."')";
    $record['usuario'] = $data['usuario']; //Numeric
    $record['trans'] = $data['trans']; //Numeric
    $record['fecha_ent'] = "CTOD('".date("H:i:s A")."')"; //Date 8

    $record['cierre'] = "''"; //Character 1
    $record['dato_1'] = "''"; //Character 30
    $record['dato_2'] = "''"; //Character 30
    $record['dato_3'] = "''"; //Character 30
    $record['dato_4'] = "''"; //Character 30
    $record['dato_5'] = "''"; //Character 30
    $record['dato_6'] = "''"; //Character 30
    $record['dato_7'] = "''"; //Character 30
    $record['dato_8'] = "''"; //Character 30
    $record['status2'] = "'Confirmado'"; //Character 15

    $record['ped_int'] = "''"; //Character 18
    $record['lugar'] = $data['lugar']; //Character 10
    $record['no_cot'] = 0; //Numeric
    $record['suc_cot'] = "''"; //Character 3
    $record['stat_pro'] = "''"; //Character 10
    $record['usu_auto'] = 73; //Numeric
    $record['pesotot'] = 0; //Numeric
    $record['cvedirent'] = "''"; //Character 6
    $record['p_y_d'] = 0; //Logical .F. .T.
    $record['descue2'] = 0; //Numeric

    $record['descue3'] = 0; //Numeric
    $record['descue4'] = 0; //Numeric
    $record['saldoanti'] = 0; //Numeric
    $record['pedref'] = "''"; //Character 1
    $record['cve_entre'] = "'0'"; //Character 5
    $record['ieps'] = 0; //Numeric
    $record['retivaped'] = 0; //Numeric
    $record['no_req'] = 0; //Numeric
    $record['suc_req'] = "''"; //Character 3
    $record['calage1'] = 1; //Numeric

    $record['calage2'] = 0; //Numeric
    $record['cve_age2'] = 0; //Numeric
    $record['cve_monm'] = 0; //Numeric
    $record['hora_ped'] = "'".date("H:i:s A")."'"; //Character 8
    $record['sig_aut'] = 0; //Numeric
    $record['tcdcto1p'] = 1; //Numeric
    $record['tcdcto2p'] = 0; //Numeric
    $record['comori'] = 1; //Numeric
    $record['retisrped'] = 0; //Numeric
    $record['cvede1'] = 0; //Numeric

    $record['cvede2'] = 0; //Numeric
    $record['cvede3'] = 0; //Numeric
    $record['cvede4'] = 0; //Numeric
    $record['cvede5'] = 0; //Numeric
    $record['cvede6'] = 0; //Numeric
    $record['cvede7'] = 0; //Numeric
    $record['cvede8'] = 0; //Numeric
    $record['usadomap'] = 0; //Numeric
    $record['savemovi'] = "''"; //Character 1
    $record['mrop'] = 0; //Numeric

    $record['folio'] = 0; //Numeric
    $record['nmretot'] = "''"; //Character 1
    $record['impto1'] = 0; //Numeric
    $record['impto2'] = 0; //Numeric
    $record['timpto1'] = 0; //Numeric
    $record['timpto2'] = 0; //Numeric
    $record['cam_auto'] = 0; //Numeric

    $sql = "Insert into pedidoc(no_ped, cve_cte, cve_age, f_alta_ped, descuento, descue, status, total_ped, impr_ped, subt_ped,
    iva, observa, cve_mon, tip_cam, cve_suc, mes, año, usuario, trans, fecha_ent, cierre, dato_1, dato_2, dato_3, dato_4, dato_5,
    dato_6, dato_7, dato_8, status2, ped_int, lugar, no_cot, suc_cot, stat_pro, usu_auto, pesotot, cvedirent, p_y_d, descue2,
    descue3, descue4, saldoanti, pedref, cve_entre, ieps, retivaped, no_req, suc_req, calage1, calage2, cve_age2, cve_monm,
    hora_ped, sig_aut, tcdcto1p, tcdcto2p, comori, retisrped, cvede1, cvede2, cvede3, cvede4, cvede5, cvede6, cvede7, cvede8,
    usadomap, savemovi, mrop, folio, nmretot, impto1, impto2, timpto1, timpto2, cam_auto) VALUES(".

        $record['no_ped'].",".
        $record['cve_cte'].",".
        $record['cve_age'].",".
        $record['f_alta_ped'].",".
        $record['descuento'].",".
        $record['descue'].",".
        $record['status'].",".
        $record['total_ped'].",".
        $record['impr_ped'].",".
        $record['subt_ped'].",".

        $record['iva'].",".
        $record['observa'].",".
        $record['cve_mon'].",".
        $record['tip_cam'].",".
        $record['cve_suc'].",".
        $record['mes'].",".
        $record['año'].",".
        $record['usuario'].",".
        $record['trans'].",".
        $record['fecha_ent'].",".

        $record['cierre'].",".
        $record['dato_1'].",".
        $record['dato_2'].",".
        $record['dato_3'].",".
        $record['dato_4'].",".
        $record['dato_5'].",".
        $record['dato_6'].",".
        $record['dato_7'].",".
        $record['dato_8'].",".
        $record['status2'].",".

        $record['ped_int'].",".
        $record['lugar'].",".
        $record['no_cot'].",".
        $record['suc_cot'].",".
        $record['stat_pro'].",".
        $record['usu_auto'].",".
        $record['pesotot'].",".
        $record['cvedirent'].",".
        $record['p_y_d'].",".
        $record['descue2'].",".

        $record['descue3'].",".
        $record['descue4'].",".
        $record['saldoanti'].",".
        $record['pedref'].",".
        $record['cve_entre'].",".
        $record['ieps'].",".
        $record['retivaped'].",".
        $record['no_req'].",".
        $record['suc_req'].",".
        $record['calage1'].",".

        $record['calage2'].",".
        $record['cve_age2'].",".
        $record['cve_monm'].",".
        $record['hora_ped'].",".
        $record['sig_aut'].",".
        $record['tcdcto1p'].",".
        $record['tcdcto2p'].",".
        $record['comori'].",".
        $record['retisrped'].",".
        $record['cvede1'].",".

        $record['cvede2'].",".
        $record['cvede3'].",".
        $record['cvede4'].",".
        $record['cvede5'].",".
        $record['cvede6'].",".
        $record['cvede7'].",".
        $record['cvede8'].",".
        $record['usadomap'].",".
        $record['savemovi'].",".
        $record['mrop'].",".

        $record['folio'].",".
        $record['nmretot'].",".
        $record['impto1'].",".
        $record['impto2'].",".
        $record['timpto1'].",".
        $record['timpto2'].",".
        $record['cam_auto'].");";

    // pre($record);
    // echo $sql;
    // die();
    // die();
    // $rs = $conn->Execute("SELECT VERSION() AS vers, ISREADONLY('pedidoc') AS ro,  no_ped FROM pedidoc");
    // pre($rs->Fields(0)->value);
    // pre($rs->Fields(1)->value);
    // pre($rs->Fields(2)->value);
    //$conn->setCharset('utf8');
    $rs = $conn->Execute($sql);
    //$rs = $conn->autoExecute('pedidoc',$record,'INSERT');
    //$rs->Close();
    // echo $rs;
}

function insertPedidod($conn, $data)
{
    $record = array();

    $record['no_ped'] = $data['no_ped']; //Numeric
    $record['cve_prod'] = $data['cve_prod']; //Character
    $record['cse_prod'] = $data['cse_prod']; //Character 10
    $record['med_prod'] = 0; //Numeric
    $record['cant_prod'] = $data['cant_prod']; //Numeric

    $record['valor_prod'] = $data['valor_prod']; //Numeric
    $record['cpm_ped'] = 0; //Numeric
    $record['fecha_ent'] = "CTOD('".date("H:i:s A")."')"; //date
    $record['status1'] = "''"; //Character 1
    $record['saldo'] = $data['saldo']; //Numeric

    $record['iva_prod'] = 16; //Numeric
    $record['factor'] = 1; //Numeric
    $record['unidad'] = "'PIEZA'"; //Character 5
    $record['cve_suc'] = $data['cve_suc']; //Character 3
    $record['trans'] = 1; //Numeric 1

    $record['conv_var'] = 0; //Numeric 1
    $record['dcto1'] = 0; //Numeric
    $record['dcto2'] = 0; //Numeric
    $record['dctotot'] = 0; //Numeric
    $record['lote'] = "''"; //Character

    $record['stat_pro'] = "''"; //Character 10
    $record['fol_prod'] = 1; //Numeric
    $record['lista_pre'] = "'2'"; //Character
    $record['cve_mon_d'] = 1; //Numeric
    $record['por_pitex'] = 0; //Numeric

    $record['caliva'] = "'TIPO'"; //Character 4
    $record['porcretiva'] = 0; //Numeric
    $record['porcenieps'] = 0; //Numeric
    $record['ieps_prod'] = 0; //Numeric
    $record['reten_iva'] = 0; //Numeric

    $record['new_med'] = $data['new_med']; //Character 6
    $record['comage1p'] = 0; //Numeric
    $record['dinage1p'] = 0; //Numeric
    $record['comage2p'] = 0; //Numeric
    $record['dinage2p'] = 0; //Numeric

    $record['prec_ant'] = 0; //Numeric
    $record['dcto1_ant'] = 0; //Numeric
    $record['dcto2_ant'] = 0; //Numeric
    $record['lista_ant'] = "''"; //Character 1
    $record['fecha_ant'] = "CTOD('".date("H:i:s A")."')"; //date

    $record['clista1'] = "''"; //Character 2
    $record['clista2'] = "''"; //Character 2
    $record['retisrpedp'] = 0; //Numeric
    $record['retisrpedd'] = 0; //Numeric
    $record['factorieps'] = 0; //Numeric

    $record['iva_ieps'] = 0; //Numeric
    $record['kpc_doc'] = 0; //Numeric
    $record['no_promo'] = 0; //Numeric
    $record['pimpto1'] = 0; //Numeric
    $record['pimpto2'] = 0; //Numeric

    $record['impto1d'] = 0; //Numeric
    $record['impto2d'] = 0; //Numeric
    $sql = "Insert into pedidod(no_ped, cve_prod, cse_prod, med_prod, cant_prod, valor_prod, cpm_ped, fecha_ent, status1, saldo,
    iva_prod,    factor,  unidad,  cve_suc, trans,   conv_var,    dcto1,   dcto2,   dctotot, lote,    stat_pro,    fol_prod,
    lista_pre,   cve_mon_d,   por_pitex,   caliva,  porcretiva,  porcenieps,  ieps_prod,   reten_iva,   new_med, comage1p, dinage1p,
    comage2p,    dinage2p,    prec_ant,    dcto1_ant,   dcto2_ant,   lista_ant,   fecha_ant,   clista1, clista2, retisrpedp,
    retisrpedd,  factorieps,  iva_ieps,    kpc_doc, no_promo,    pimpto1, pimpto2, impto1d, impto2d) values(".
        $record['no_ped'].",".
        $record['cve_prod'].",".
        $record['cse_prod'].",".
        $record['med_prod'].",".
        $record['cant_prod'].",".
        $record['valor_prod'].",".
        $record['cpm_ped'].",".
        $record['fecha_ent'].",".
        $record['status1'].",".
        $record['saldo'].",".
        $record['iva_prod'].",".
        $record['factor'].",".
        $record['unidad'].",".
        $record['cve_suc'].",".
        $record['trans'].",".
        $record['conv_var'].",".
        $record['dcto1'].",".
        $record['dcto2'].",".
        $record['dctotot'].",".
        $record['lote'].",".
        $record['stat_pro'].",".
        $record['fol_prod'].",".
        $record['lista_pre'].",".
        $record['cve_mon_d'].",".
        $record['por_pitex'].",".
        $record['caliva'].",".
        $record['porcretiva'].",".
        $record['porcenieps'].",".
        $record['ieps_prod'].",".
        $record['reten_iva'].",".
        $record['new_med'].",".
        $record['comage1p'].",".
        $record['dinage1p'].",".
        $record['comage2p'].",".
        $record['dinage2p'].",".
        $record['prec_ant'].",".
        $record['dcto1_ant'].",".
        $record['dcto2_ant'].",".
        $record['lista_ant'].",".
        $record['fecha_ant'].",".
        $record['clista1'].",".
        $record['clista2'].",".
        $record['retisrpedp'].",".
        $record['retisrpedd'].",".
        $record['factorieps'].",".
        $record['iva_ieps'].",".
        $record['kpc_doc'].",".
        $record['no_promo'].",".
        $record['pimpto1'].",".
        $record['pimpto2'].",".
        $record['impto1d'].",".
        $record['impto2d'].");";
    $rs = $conn->Execute($sql);
}

function validateParams($allPostPutVars)
{
    $cve_cte = isset($allPostPutVars['data']['cve_cte']) ? $allPostPutVars['data']['cve_cte'] : null;
    $cve_age = isset($allPostPutVars['data']['cve_age']) ? $allPostPutVars['data']['cve_age'] : null;
    $pedidos = isset($allPostPutVars['data']['pedidos']) ? $allPostPutVars['data']['pedidos'] : null;
    if (!($cve_cte && is_numeric($cve_cte))) {
        return "Cve_cte incorrecto";
    }
    if (!($cve_age && is_numeric($cve_age))) {
        return "Cve_age incorrecto";
    }
    $i = 1;
    if (count($pedidos) > 0) {
        foreach ($pedidos as $pedido) {
            if (!strlen($pedido['cve_prod']) > 0) {
                return "cve_prod del pedido #".$i." incorrecto";
            }
            if (!($pedido['cantidad'] > 0 && is_numeric($pedido['cantidad']))) {
                return "cantidad del pedido #".$i." incorrecto";
            }
            $i++;
        }
    } else {
        return "no hay pedidos";
    }
    return "OK";
}

function getNextPedidocId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 no_ped from pedidoc order by no_ped desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getNextMovsId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 no_mov from movs order by no_mov desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getFirst($connection, $table, $column)
{
    $results = $connection->Execute("select top 1 * from {$table} order by {$column} desc;");
    $payload = [];
    $columnsCount = $results->Fields->Count;

    if ($columnsCount > 0) {
        $columnIndex = 0;

        while (!$results->EOF) {
            for ($index = 0; $index < $columnsCount; $index++) {
                $payload[$columnIndex][$results->Fields($index)->Name] = trim(utf8_encode($results->Fields($index)));
            }

            $columnIndex++;

            $results->MoveNext();
        }
    }

    $results->Close();

    return $payload;
}
