<?php

namespace App\Http\Controllers\Api\V1;

use App\Database\Connectors\VFPConnector;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

class OrdersController extends Controller
{
    public function show($orderId)
    {
        $data = [];
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = "select * from pedidoc where no_ped = {$orderId}";
        $result = getTableData($conn, $sql);

        $sql = "select * from pedidod where no_ped = {$orderId}";
        $result2 = getTableData($conn, $sql);

        if ($result) {
            $data = [
                'data' => [
                    'cabecera' => $result,
                    'detalle' => $result2,
                ],
            ];
        } else {
            $data = [
                'data' => [
                    'status' => 400,
                    'descripcion' => 'Ocurrio un error en el request',
                ],
            ];
        }

        return $data;
    }

    public function invoices($orderId, $format)
    {
        if (!in_array($format, ['pdf', 'xml'])) {
            return response('', 404);
        }

        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('facturad')->where('no_ped', (int) $orderId));
        $data = getTableData($conn, "select top 1 * from facturad where no_ped = {$orderId} order by no_fac desc;");

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $record = new Fluent($record);

        $sql = dump_sql(DB::table('factuxml')->where('no_fac', $record->no_fac));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $record = new Fluent($record);

        $invoiceDate = explode('_', $record->nomxml_fac)[2];
        $invoiceYear = substr($invoiceDate, 0, 4);
        $invoiceMonth = substr($invoiceDate, 4, 2);
        $invoiceDay = substr($invoiceDate, 6, 2);

        $format = Str::upper($format);

        $invoiceFileName = Str::before($record->nomxml_fac, '.');
        $invoiceFile = "{$invoiceYear}\\{$invoiceMonth}\\{$invoiceDay}\\{$invoiceFileName}.{$format}";
        $invoiceFilePath = config('app.invoices_repository_path').$invoiceFile;

        return response()->file($invoiceFilePath);
    }
}
