<?php

namespace App\Http\Controllers\Api\V1;

use App\Database\Connectors\VFPConnector;
use App\Filters\CustomerFilters;
use App\Http\Controllers\Controller;
use App\Services\Sai\Models\Clientes;
use App\Services\Sai\Models\Clienxml;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;

class CustomersController extends Controller
{
    public function index(CustomerFilters $filters)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql($filters->apply(DB::table('clientes')));
        $data = getTableData($conn, $sql);

        return $data;
    }

    public function store()
    {
        request()->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'zone' => ['required', 'integer'],
            'subzone' => ['required', 'integer'],
            'distribution_channel' => ['required', 'integer'],
            'type_id' => ['required', 'string'],
            'prices_list' => ['required'],
            'address' => ['nullable'],
            'neighborhood' => ['nullable'],
            'city' => ['nullable'],
            'state' => ['nullable'],
            'zip_code' => ['nullable'],
            'country' => ['nullable'],
            'exterior_number' => ['nullable'],
            'interior_number' => ['nullable'],
            'municipality' => ['nullable'],
            'phone' => ['nullable'],
            'tel2_cte' => ['nullable'],
            'company_website' => ['nullable'],
            'billing_uso_cfdi' => ['nullable'],
            'billing_bussiness_name' => ['nullable'],
            'billing_city' => ['nullable'],
            'billing_neighborhood' => ['nullable'],
            'billing_zip_code' => ['nullable'],
            'billing_municipality' => ['nullable'],
            'billing_address' => ['nullable'],
            'billing_state' => ['nullable'],
            'billing_country' => ['nullable'],
            'billing_rfc' => ['nullable'],
        ]);

        $con = new VFPConnector();
        $conn = $con->getConnection();

        $data = getTableData($conn, "Select MAX(cve_cte) from clientes;");
        $maxId = (int) Arr::first(Arr::first($data));
        $customerId = (int) getNextConfigId($conn, 'no_mov', 'CTE');

        $sql = dump_command_sql(DB::table('clientes'), 'Insert', (new Clientes([
            'cve_cte' => $customerId,

            'nom_cte' => request('name'),
            'cve_zon' => (int) request('zone'),
            'cve_sub' => (int) request('subzone'),
            'cve_can' => request('distribution_channel'),
            'cve_iva' => request('type_id'),
            'email_cte' => request('email'),
            'tel1_cte' => request('phone', ''),
            'tel2_cte' => request('tel2_cte', ''),

            'lista_prec' => (int) request('prices_list'),

            'dir_cte' => request('address', ''),
            'col_cte' => request('neighborhood', ''),
            'cd_cte' => request('city', ''),
            'edo_cte' => request('state', ''),
            'cp_cte' => request('zip_code', ''),
            'pais_cte' => request('country', ''),
            'ne_cte' => request('exterior_number', ''),
            'ni_cte' => request('interior_number', ''),
            'delega_cte' => request('municipality', ''),

            // default
            'www_cte' => request('company_website', ''),
            'nom_ent' => request('name'),
            'falta_cte' => "\\CTOD('".date("m-d-Y")."')",

            'nom_fac' => request('billing_bussiness_name', request('name', '')),
            'uso_cte' => request('billing_uso_cfdi', ''),
            'cd_fac' => request('billing_city', ''),
            'col_fac' => request('billing_neighborhood', ''),
            'cp_fac' => request('billing_zip_code', ''),
            'delega_fac' => request('billing_municipality', ''),
            'dir_fac' => request('billing_address', ''),
            'edo_fac' => request('billing_state', ''),
            'pais_fac' => request('billing_country', ''),
            'rfc_cte' => request('billing_rfc', 'XAXX010101000'),
        ]))->toArray());

        $data = $conn->Execute($sql);

        $sql = dump_command_sql(DB::table('clienxml'), 'Insert', (new Clienxml([
            'cve_cte' => $customerId,
        ]))->toArray());
        $data = $conn->Execute($sql);

        $sql = dump_command_sql(DB::table('config')->whereRaw("clave = 'CTE'"), 'Update', [
            'no_mov' => $customerId + 1,
        ]);
        $data = $conn->Execute($sql);

        return response()->json([
            'id' => (int) $customerId,
        ], 201);
    }

    public function show($id)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('clientes')->where('cve_cte', (int) $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $customer = new Fluent($record);

        $sql = dump_sql(DB::table('clienxml')->where('cve_cte', (int) $id));
        $data = getTableData($conn, $sql);

        $customer->clienxml = Arr::first($data);

        return $customer;
    }

    public function update($id)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('clientes')->where('cve_cte', (int) $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $data = request()->validate([
            'name' => ['sometimes', 'required'],
            'email' => ['sometimes', 'required', 'email'],
            'zone' => ['sometimes', 'required', 'integer'],
            'subzone' => ['sometimes', 'required', 'integer'],
            'distribution_channel' => ['sometimes', 'required', 'integer'],
            'type_id' => ['sometimes', 'required', 'string'],
            'prices_list' => ['sometimes', 'required'],
            'address' => ['nullable'],
            'neighborhood' => ['nullable'],
            'city' => ['nullable'],
            'state' => ['nullable'],
            'zip_code' => ['nullable'],
            'country' => ['nullable'],
            'exterior_number' => ['nullable'],
            'interior_number' => ['nullable'],
            'municipality' => ['nullable'],
            'phone' => ['nullable'],
            'tel2_cte' => ['nullable'],
            'www_cte' => ['nullable'],

            'billing_uso_cfdi' => ['nullable'],
            'billing_bussiness_name' => ['nullable'],
            'billing_city' => ['nullable'],
            'billing_neighborhood' => ['nullable'],
            'billing_zip_code' => ['nullable'],
            'billing_municipality' => ['nullable'],
            'billing_address' => ['nullable'],
            'billing_state' => ['nullable'],
            'billing_country' => ['nullable'],
            'billing_rfc' => ['nullable'],
        ]);

        $dataKeys = [];
        $equivalentKeys = [
            'name' => [
                'nom_cte',
                'nom_ent',
            ],
            'zone' => [
                'cve_zon',
            ],
            'subzone' => [
                'cve_sub',
            ],
            'distribution_channel' => [
                'cve_can',
            ],
            'type_id' => [
                'cve_iva',
            ],
            'email' => [
                'email_cte',
            ],
            'phone' => [
                'tel1_cte',
            ],
            'tel2_cte' => [
                'tel2_cte',
            ],
            'prices_list' => [
                'lista_prec',
            ],
            'address' => [
                'dir_cte',
            ],
            'neighborhood' => [
                'col_cte',
            ],
            'city' => [
                'cd_cte',
            ],
            'state' => [
                'edo_cte',
            ],
            'zip_code' => [
                'cp_cte',
            ],
            'country' => [
                'pais_cte',
            ],
            'exterior_number' => [
                'ne_cte',
            ],
            'interior_number' => [
                'ni_cte',
            ],
            'municipality' => [
                'delega_cte',
            ],
            'company_website' => [
                'www_cte',
            ],
            'billing_bussiness_name' => [
                'nom_fac',
            ],
            'billing_uso_cfdi' => [
                'uso_cte',
            ],
            'billing_city' => [
                'cd_fac',
            ],
            'billing_neighborhood' => [
                'col_fac',
            ],
            'billing_zip_code' => [
                'cp_fac',
            ],
            'billing_municipality' => [
                'delega_fac',
            ],
            'billing_address' => [
                'dir_fac',
            ],
            'billing_state' => [
                'edo_fac',
            ],
            'billing_country' => [
                'pais_fac',
            ],
            'billing_rfc' => [
                'rfc_cte',
            ],
        ];

        foreach ($data as $key => $value) {
            foreach ($equivalentKeys[$key] as $keyName) {
                $dataKeys[$keyName] = true;
            }
        }

        $sql = dump_command_sql(DB::table('clientes')->whereRaw("cve_cte = {$id}"), 'Update', array_intersect_key([
            'nom_cte' => request('name'),
            'cve_zon' => (int) request('zone'),
            'cve_sub' => (int) request('subzone'),
            'cve_can' => request('distribution_channel'),
            'cve_iva' => request('type_id'),
            'email_cte' => request('email'),
            'tel1_cte' => request('phone', ''),
            'tel2_cte' => request('tel2_cte', ''),

            'lista_prec' => (int) request('prices_list'),

            'dir_cte' => request('address', ''),
            'col_cte' => request('neighborhood', ''),
            'cd_cte' => request('city', ''),
            'edo_cte' => request('state', ''),
            'cp_cte' => request('zip_code', ''),
            'pais_cte' => request('country', ''),
            'ne_cte' => request('exterior_number', ''),
            'ni_cte' => request('interior_number', ''),
            'delega_cte' => request('municipality', ''),

            // default
            'www_cte' => request('company_website', ''),
            'nom_ent' => request('name'),

            'nom_fac' => request('billing_bussiness_name', request('name', '')),
            'uso_cte' => request('billing_uso_cfdi', ''),
            'cd_fac' => request('billing_city', ''),
            'col_fac' => request('billing_neighborhood', ''),
            'cp_fac' => request('billing_zip_code', ''),
            'delega_fac' => request('billing_municipality', ''),
            'dir_fac' => request('billing_address', ''),
            'edo_fac' => request('billing_state', ''),
            'pais_fac' => request('billing_country', ''),
            'rfc_cte' => request('billing_rfc', 'XAXX010101000'),
        ], $dataKeys));

        $data = $conn->Execute($sql);

        return response()->json([
            'id' => (int) $id,
        ], 200);
    }

    public function destroy($id)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('clientes')->where('cve_cte', (int) $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $sql = dump_delete_sql(DB::table('clientes')->whereRaw("cve_cte = {$id}"));
        $data = $conn->Execute($sql);

        $sql = dump_delete_sql(DB::table('clienxml')->whereRaw("cve_cte = {$id}"));
        $data = $conn->Execute($sql);

        return response('', 204);
    }
}
