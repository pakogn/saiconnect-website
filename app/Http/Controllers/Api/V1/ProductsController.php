<?php

namespace App\Http\Controllers\Api\V1;

use App\Database\Connectors\VFPConnector;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;

class ProductsController extends Controller
{
    public function index()
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto'));
        $data = getTableData($conn, $sql);

        return $data;
    }

    public function show($id)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $product = new Fluent($record);

        $sql = dump_sql(DB::table('existe')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);
        $product->existencias = collect($data)->groupBy('lugar');

        $sql = dump_sql(DB::table('prodmed')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);
        $product->precios = $data;

        return $product;
    }

    public function showWithInventory($id, $model)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $product = new Fluent($record);

        $sql = dump_sql(DB::table('existe')->where('cve_prod', $id)->where('new_med', 'like', "%{$model}%"));
        $data = getTableData($conn, $sql);
        $product->existencias = collect($data)->groupBy('lugar');

        $sql = dump_sql(DB::table('prodmed')->where('cve_prod', $id)->where('new_med', 'like', "%{$model}%"));
        $data = getTableData($conn, $sql);
        $product->precios = $data;

        return $product;
    }
}
