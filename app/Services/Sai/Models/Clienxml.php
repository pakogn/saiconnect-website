<?php

namespace App\Services\Sai\Models;

use Illuminate\Database\Eloquent\Model;

class Clienxml extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'cve_cte' => -1,
        'envioxmail' => 0,
        'envipdfxml' => 3,
        'sinconencr' => 0,
        'addaddenda' => 0,
        'certificte' => '',
        'addendacte' => '',
        'addendadev' => '',
        'addendades' => '',
        'copiarxml' => 0,
        'adondecopy' => 1,
        'rutalterna' => '',
        'compledeta' => 0,
        'addnames' => 0,
        'namespace' => '',
        'copypdfxml' => 2,
        'nocreapdf' => 0,
        'yevacondi' => 0,
        'conddepago' => 0,
        'vercfdespe' => 0,
        'version_02' => 0,
        'c2metopago' => '',
        'c2motidesc' => '',
        'c2nctapago' => '',
        'graloespe' => 1,
        'confirespe' => 0,
        'c2metodosu' => '',
        'c2condisu' => '',
        'c2cuentasu' => '',
        'c2descuesu' => '',
        'armaxmlcte' => '',
        'ordexmlcte' => '',
        'ucatmetpag' => 0,
        'quemetpag' => '',
        'resi_cte' => '',
        'resi_fac' => '',
        'regi_cte' => '',
        'uso_cte' => '',
        'metp_cte' => '',
        'fcusosat' => '',
        'vcusosat' => '',
        'scusosat' => '',
        'ncusosat' => '',
        'kitpar' => 0,
        'kitparpi' => 0,
        'sug_uso' => 0,
        'uti_ofis' => 0,
        'rfc_ofi' => '',
        'nom_ofi' => '',
        'dir_ofi' => '',
        'ne_ofi' => '',
        'ni_ofi' => '',
        'col_ofi' => '',
        'cd_ofi' => '',
        'delega_ofi' => '',
        'edo_ofi' => '',
        'resi_ofi' => '',
        'cp_ofi' => '',
        'coor_ofi' => '',
        'cmetpsat' => '',
        'ometpsat' => '',
        'pmetpsat' => '',
        'gralcteco' => 0,
        'copiaxmlrc' => 0,
        'adoncopyrc' => 0,
        'copdfxmlrc' => 0,
        'ralternarc' => '',
        'envxmailrc' => 0,
        'envpdfxmlc' => 0,
        'folpagnumc' => 0,
        'digpagnumc' => 0,
    ];
}
