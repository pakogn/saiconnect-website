<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

abstract class Filters
{
    /**
     * The HTTP Request.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The Eloquent builder.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $builder;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Create a new ThreadFilters instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->{$filter}($value);
            }
        }

        if (method_exists($this, 'finalize')) {
            $this->finalize();
        }

        return $this->builder;
    }

    /**
     * Fetch all relevant filters from the request.
     *
     * @return array
     */
    public function getFilters()
    {
        return collect(array_filter($this->request->only($this->filters)));
    }

    /**
     * Retrieve a filter item by key.
     *
     * @return mixed|null
     */
    public function get($key)
    {
        return isset($this->getFilters()[$key]) ? $this->getFilters()[$key] : null;
    }

    public function issetDateRage()
    {
        if (is_null($this->get('start_date'))) {
            $this->request->offsetUnset('end_date');
            return false;
        }

        return true;
    }

    public function getDateRange()
    {
        if (empty($this->get('end_date'))) {
            $this->request->offsetSet('end_date', Carbon::now()->endOfDay());
            $endDate = Carbon::now()->endOfDay();
        } else {
            $endDate = Carbon::parse($this->get('end_date'))->endOfDay();
        }

        return [Carbon::parse($this->get('start_date'))->startOfDay(), $endDate];
    }
}
