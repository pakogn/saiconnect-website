<?php

use App\Database\Connectors\VFPConnector;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Fluent;

$saiTables = ['agentes', 'AGRSAT', 'agru_est', 'agructos', 'agrupacot', 'agrupad', 'agrupcot', 'akit_nvt', 'ALMADERE', 'almsfrxs', 'analisis', 'anpagos2', 'anventac', 'anventad', 'arbolc', 'arch_xls', 'asegchp', 'asigreac', 'asigread', 'atributc', 'ATRIBUTO', 'auto_oc', 'AUTO_PED', 'auto_req', 'bancossat', 'bitacfds', 'bitecom', 'bitecta', 'biteempl', 'boletad', 'boletas', 'borra', 'buscador', 'busqnomv', 'calapis', 'calenlac', 'calevent', 'calexcep', 'camion', 'canald', 'caracpcon', 'caracprd', 'carentr', 'cargodoc', 'cargoxm2', 'cargoxml', 'carinico', 'carinive', 'carpdoc', 'cat_con', 'cat_est', 'cat_gpo', 'cat_map', 'cataenti', 'catasatc', 'catasatd', 'catentr2', 'CATENTRE', 'catetrab', 'causas', 'ccdenomi', 'ccretiro', 'cencosal', 'centcost', 'certcacp', 'cfd', 'cfdirelac', 'cfgrepbs', 'CFGREPCA', 'CFGREPEN', 'cfgrepor', 'CFGREPTA', 'cfgrepwh', 'checcfdi', 'cheq', 'chklistc', 'chklistp', 'chklistv', 'chofer', 'cierdme2', 'cierdmes', 'clases', 'clasific', 'clieimag', 'CLIENTES', 'clienxml', 'cnfgdos', 'cnfpd_vt', 'coagxcon', 'COAGXCTE', 'coagxdef', 'coagxfac', 'coagxli2', 'coagxlis', 'COAGXPED', 'coagxpro', 'coagxrem', 'codbarcf', 'COLORNTA', 'colortot', 'COM_AGE', 'comagcl', 'comagpd', 'compdoc', 'compradc', 'comprafc', 'comprafd', 'comprapc', 'COMPRAPD', 'comproc', 'comprod', 'con_cve', 'con_est', 'con_fol', 'concc', 'concd', 'conceptos', 'concoc', 'concod', 'concp', 'concr', 'conddeta', 'conddocu', 'condentr', 'confcont', 'confdeca', 'confdeec', 'conficpbi', 'confiecom', 'config', 'config1', 'CONFIMP2', 'confimpo', 'conflote', 'consdeta', 'consdocu', 'consemp', 'consentr', 'consignc', 'consignd', 'conspoli', 'CONTACTO', 'CONTADO', 'contdmes', 'contened', 'contpro', 'CONTPROS', 'controle', 'controlp', 'cortcajc', 'cortcajd', 'costeoc', 'costeod', 'costeos', 'cotcdeta', 'cotcdoc', 'cotcompc', 'cotcompd', 'COTIDETA', 'cotidoc', 'cotiespc', 'cotiespd', 'cotizac', 'COTIZAD', 'cotper', 'cred2d', 'credidoc', 'credito2', 'creditod', 'creditos', 'credixm2', 'credixml', 'credxmld', 'creinico', 'creinive', 'crepdoc', 'cta_conc', 'cta_ctes', 'cta_imp', 'cta_proc', 'cta_proc2', 'cta_proc3', 'cta_prod', 'cta_prop', 'ctadepto', 'ctaplazo', 'ctas_aso', 'ctas_ext', 'ctasclie', 'ctascont', 'ctasempl', 'ctasgen', 'ctasprov', 'ctativac', 'ctativap', 'ctecargo', 'CTECLABE', 'ctecon', 'ctecot', 'ctecredi', 'ctedcon', 'ctedir', 'ctefac', 'cteped', 'cterem', 'ctesfrxs', 'ctexclas', 'CTEXPROD', 'cuentas', 'cuentref', 'cvconv', 'cvconvc', 'cvconvi', 'cvconvp', 'cveanti', 'cvetras', 'cvpoli', 'cvresp', 'cvtemas', 'cvtemasp', 'daestag1', 'daestag2', 'daestag3', 'daestag4', 'daestal1', 'daestal2', 'daestal3', 'daestal4', 'daestcl1', 'daestcl2', 'daestcl3', 'daestcl4', 'daestdc', 'daestdc1', 'daestdc2', 'daestdc3', 'daestdc4', 'daestdc5', 'daestdc6', 'daestdc7', 'daestdc8', 'daestem1', 'daestem2', 'daestem3', 'daestem4', 'daestpd1', 'daestpd2', 'daestpd3', 'daestpd4', 'daestpv1', 'daestpv2', 'daestpv3', 'daestpv4', 'dcagxcon', 'dcagxfac', 'dcagxped', 'dcagxrem', 'dctocoag', 'ddacon', 'ddadeta', 'ddadocu', 'ddaentr', 'ddpdeta', 'denbill', 'denmone', 'departa', 'deposic', 'deposid', 'DERECHOS', 'derepend', 'dermovil', 'des_etiq', 'descxalm', 'destdir', 'devconsc', 'devconsd', 'diascli', 'doc_comc', 'doc_comd', 'doc_coml', 'docscte', 'docsempl', 'docsprod', 'docspros', 'docsprov', 'ecommer', 'emailsai', 'embarcar', 'empleados', 'empresas', 'equipfot', 'equipos', 'estadcat', 'estadpro', 'estcdeta', 'estcdoc', 'estimac', 'estimad', 'estseg', 'etiq_cot', 'etiq_oco', 'etiqprod', 'exclalm', 'exclcte', 'excllug', 'exclprov', 'exi_auto', 'existe', 'EXISTENCIA_20160531_CANTIDADOCOSTONEGATIVO', 'externas', 'facddeta', 'facinico', 'facinive', 'facorser', 'facpdoc', 'facrecc', 'facrecd', 'facreco', 'factentr', 'factudoc', 'facturac', 'facturad', 'factuxm2', 'factuxml', 'factxmld', 'fallas', 'faltasob', 'fcomentr', 'fconcol', 'fconenvioc', 'fconenviod', 'FCONINF', 'FCONORD', 'fcontab', 'festejos', 'filtros', 'flete', 'flujef_r', 'folcajpr', 'folcajve', 'folcarco', 'folcarve', 'folcheco', 'folconal', 'folconci', 'folconcu', 'folcotco', 'folcotes', 'folcotve', 'folcreco', 'folcreve', 'foldevco', 'folestco', 'folfacco', 'FOLFACVE', 'foliosua', 'folmresp', 'folnotav', 'folnotve', 'folordco', 'folordpr', 'folpedve', 'folrefpr', 'folremve', 'folreqco', 'folrespr', 'folstran', 'fondoin', 'frases', 'genera', 'gentxt', 'graficas', 'GRAFPLAN', 'gruposc', 'guiad', 'guias', 'guietiq', 'hisllave', 'hojaprod', 'homepage', 'horextc', 'horextd', 'htmayum', 'icliente', 'iconfvta', 'icotcdeta', 'icotcompc', 'icotcompd', 'icotidet', 'icotizac', 'icotizad', 'imagnta', 'imgform', 'imptos', 'incaxmld', 'indexa', 'ipedidet', 'ipedidoc', 'ipedidod', 'ipedient', 'iprodent', 'istoage', 'istocte', 'istofol', 'istofolp', 'istoprod', 'istoprov', 'kit', 'kit_con', 'kit_cot', 'kit_dec', 'kit_dev', 'kit_fac', 'kit_iped', 'kit_nvta', 'kit_ped', 'kit_rem', 'lecturac', 'lecturad', 'lecturas', 'localiza', 'lote', 'loteod', 'lotesug', 'LUGAALMA', 'lugares', 'mantdeta', 'mantenim', 'mantrefa', 'manttare', 'mapac', 'mapad', 'masivap', 'masivsat', 'mes_crre', 'metodos', 'metp_sat', 'METPAGO', 'modmovil', 'modulos', 'monecfdi', 'MONEDAS', 'monedero', 'moneelec', 'monemovs', 'monsat', 'montdcto', 'montosoc', 'montosrq', 'mordproc', 'mordprod', 'mortizac', 'mortizad', 'motivosc', 'MOVCHE', 'movdetas', 'movdocu', 'movnomc', 'movnomd', 'movrep', 'movs', 'movsalad', 'movsca', 'movsdeta', 'movsentr', 'movserv', 'movsetiq', 'msgsprod', 'ncarclie', 'ncargc', 'ncargv', 'ncarprov', 'ncreclie', 'ncreprov', 'nomacum', 'nomacumd', 'nomaguin', 'nomaguinc', 'nomaguind', 'nomasdic', 'nomasdid', 'nomasdiv', 'nomcaisrc', 'nomcaisrd', 'nomcat', 'nomcat1', 'nomdetper', 'nomfiniq', 'nomform', 'nominc', 'nominc1', 'nomincap', 'nomincc', 'nomincd', 'nomisr', 'nomixm2', 'nomixml', 'nompoli', 'nomptuc', 'nomptud', 'nompues', 'nomsainc', 'nomsaind', 'nomsalmin', 'nomsolva', 'nomsolvad', 'nomsubsidi', 'nomtbls', 'nomtipop', 'nomturno', 'nomvacac', 'nomvacacd', 'notapros', 'notas', 'notclien', 'notidoc', 'notpdoc', 'notpro', 'notvdoc', 'npagos2', 'nventasc', 'nventasd', 'nvtaetiq', 'ocomdeta', 'odatlsp1', 'odatlsp2', 'odatlsp3', 'odatlsp4', 'odatlsp5', 'odatlsp6', 'odatlsp7', 'odatlsp8', 'odatlspc', 'odncetiq', 'ODPEREFA', 'opfadeta', 'opro', 'oprod', 'oprodeta', 'oprodocu', 'oproentr', 'ord_entr', 'orddatos', 'ordproc', 'ordprod', 'ordresau', 'ordrespr', 'oreqdeta', 'otmantde', 'otmante', 'otrefacc', 'otrocomp', 'ottareas', 'pacsd', 'pag2deta', 'pag2docu', 'pag2obs', 'pagcdeta', 'pagcdocu', 'pagcobs', 'pago_age', 'pagoc', 'pagocxml', 'pagos2', 'pagoxm2', 'pagoxmlc', 'pagoxmld', 'pagxdeta', 'pagxdocu', 'pagxobs', 'panelc', 'paneld', 'pantayu', 'peddocu', 'pedexmld', 'PEDIDETA', 'pedidoc', 'PEDIDOD', 'pedidod2', 'PEDIENTR', 'plantic', 'plantid', 'plazos', 'pol_mens', 'polconte', 'POLI_MOV', 'politic2', 'politica', 'polizas', 'polxml', 'porcxdia', 'posfechc', 'POSFECHD', 'prdcamco', 'prdcamcom', 'prds_sat', 'preminv', 'prenomc', 'prenomd', 'prenome', 'prenomh', 'presctas', 'presta', 'prestamo', 'prmfilt', 'prmgrids', 'pro_ctac', 'pro_ctac2', 'pro_ctac3', 'pro_ctap', 'PRO_CTAS', 'pro_desc', 'prod_zap', 'prodana', 'prodequi', 'prodetiq', 'PRODIMAG', 'prodmax', 'PRODMED', 'prodproc', 'producto', 'PRODUNID', 'prog_aut', 'progproc', 'progprod', 'promoc', 'promod', 'promos', 'promox', 'pronvend', 'propieda', 'propprod', 'PROSPECT', 'prosxcla', 'provedor', 'provimag', 'PROVXPRO', 'prvclabe', 'prvsfrxs', 'pxmldoc', 'ranansa', 'ranc_ieps', 'ranc_imp1', 'ranc_imp2', 'ranc_iva', 'ranc_risr', 'ranc_riva', 'ranconta', 'rangopag', 'rangxalm', 'ranp_ieps', 'ranp_iimp', 'ranp_imp1', 'ranp_imp2', 'ranp_iva', 'ranp_risr', 'ranp_riva', 'recnomc', 'recnomd', 'recnome', 'REGIMENS', 'rela_sat', 'remc', 'remd', 'remddeta', 'rementr', 'remidoc', 'reqcdoc', 'requi_c', 'requi_d', 'res_ana', 'resanac', 'resdocu', 'responeq', 'restring', 'resulptu', 'rfc_sub', 'riesgos', 'rprodeta', 'rprodocu', 'ruta_oc', 'ruta_ped', 'ruta_req', 'rutinas', 'rwsrcanp', 'sainalm2', 'sainalma', 'sainctas', 'saldos', 'satws', 'secuenci', 'secxusu', 'segcoti', 'segsec', 'seguicxc', 'separado', 'sepdeta', 'seriedig', 'sernodig', 'setsaimv', 'sgmtoc', 'soliproc', 'soliprod', 'srvcata', 'srvcatp', 'srvchkl', 'srvconf', 'srvcotde', 'srvcotic', 'srvcotid', 'srvcteeq', 'srvcteor', 'srvdatv', 'srvdocu', 'srventr', 'srveqent', 'srvequid', 'srvequif', 'srvequip', 'srvfacc', 'srvfacd', 'srvfacde', 'srvfilt', 'srvgrids', 'srvochk', 'srvofot', 'srvoodt', 'srvordc', 'srvprocs', 'srvproct', 'srvsnot', 'srvsord', 'srvsurde', 'srvsurtd', 'srvxcoag', 'srvxdcag', 'stradeta', 'straentr', 'strandoc', 'stransfc', 'stransfd', 'SUB2CLAS', 'SUBCLASE', 'subtipos', 'subzonas', 'sucursal', 't_consig', 't_ncargo', 't_ncred', 'tab_com', 'TABCOMAG', 'tareas', 'tc_crre', 'tcdiario', 'temas', 'texaviso', 'texcarta', 'textocot', 'texxmld', 'themesd', 'themesh', 'tiplotes', 'tipmovcu', 'tipoalma', 'tipocam', 'tipocont', 'tipoequ', 'tipofal', 'TIPOIVAC', 'tipoivap', 'tipotrab', 'tipprocl', 'tipseg', 'tivanvta', 'TPOIVACA', 'tpoivapa', 'trabajad', 'trans_er', 'transban', 'transext', 'transini', 'transmi', 'txtpreci', 'typetxt', 'ucredcte', 'unidad', 'unidpeso', 'unis_sat', 'uso_sat', 'usoctecs', 'usoctecse', 'USUAALM', 'USUALMAU', 'USUALMSR', 'USUALMST', 'USUARIO2', 'USUARIOS', 'varclpd', 'vcompra', 'vcredito', 'vencidos', 'vital', 'vncargo', 'vpagosc', 'wsrcanp', 'zonas'];

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/')
    ->uses('HomeController@index')
    ->name('index');

Route::get('distribution-channels')
    ->uses('DistributionChannelsController@index')
    ->name('distribution-channels.index');

Route::get('customer-types')
    ->uses('CustomerTypesController@index')
    ->name('customer-types.index');

Route::get('zones')
    ->uses('ZonesController@index')
    ->name('zones.index');

Route::get('subzones')
    ->uses('SubzonesController@index')
    ->name('subzones.index');

Route::apiResource('customers', 'CustomersController');

Route::get('products')
    ->uses('ProductsController@index')
    ->name('products.index');

Route::get('products/{product}')
    ->uses('ProductsController@show')
    ->name('products.show');
Route::get('products/{product}/{model}')
    ->uses('ProductsController@showWithInventory')
    ->name('products.show-with-inventory');

Route::get('orders/{id}', function ($id) {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();
    $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    $result = getTableData($conn, $sql);

    $sql = 'select * from pedidod where no_ped = '.$id;
    $result2 = getTableData($conn, $sql);

    $code = 200;

    if ($result) {
        $data = $result;
    } else {
        $code = 400;
        $data = [
            'data' => [
                'status' => $code,
                'description' => 'Ocurrio un error en el request',
            ],
        ];
    }

    return $data;
});

Route::get('check', function () use ($saiTables) {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $errors = [];

    foreach ($saiTables as $saiTable) {
        try {
            $sql = dump_sql(DB::table($saiTable)->selectRaw('COUNT(*)'));
            $result = getTableData($conn, $sql);
            $data[$saiTable] = $result;
        } catch (Exception $e) {
            $errors[] = $saiTable;
        }
    }

    return [
        'data' => $data,
        'errors' => $errors,
    ];
});

Route::get('check2', function () {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    // dd(getNextConfigId($conn, 'no_mov', 'CTE'));

    $errors = [];

    $saiTables = [
        // 'config',
        // 'USUARIO2',
        // 'USUARIOS',
        // 'srvgrids',
        // 'CTASCLIE',
        // 'canald',
        'config',
    ];

    foreach ($saiTables as $saiTable) {
        try {
            // $sql = dump_sql(DB::table($saiTable)->limit(20)->orderBy('cve_cte', 'desc'));
            // $sql = dump_sql(DB::connection('sqlsrv')->table($saiTable)->where('clave', 'CTE'));
            $sql = dump_sql(DB::table($saiTable));
            $result = getTableData($conn, $sql);
            $data[$saiTable] = $result;
        } catch (Exception $e) {
            Log::debug($e);

            $errors[] = $saiTable;
        }
    }

    return [
        'data' => $data,
        'errors' => $errors,
    ];
});

Route::get('tabla/{table}/{column}', function ($saiTable, $column) {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $data = getFirst($conn, $saiTable, $column);

    return $data;
});

Route::get('check3', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $lastId = "";
    $rs = $conn->Execute("select top 1 no_mov from movs order by no_mov desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();
    return (string) $lastId;
});

Route::get('pedidos', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = dump_sql(DB::table('pedidoc'));
    $data = getTableData($conn, $sql);

    return $data;
});

Route::get('pedidos/{order}/invoices/{format}')
    ->uses('OrdersController@invoices')
    ->name('orders.invoices.file');

Route::get('pedidos/porsurtir', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = "Select no_ped, status from pedidoc where lugar = 'MTY' and status = 'Por Surtir' and no_ped >= 179720";
    $data = getTableData($conn, $sql);

    return $data;
});

Route::get('pedidos/{id}', function ($id) {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);

    if (is_null($record = Arr::first($data))) {
        return response('', 404);
    }

    $order = new Fluent($record);

    $sql = dump_sql(DB::table('pedidod')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);
    $order->lines = collect($data);

    return $order;
});

Route::post('pedidos', function () {
    $code = 200;
    $allPostParams = request()->all();
    $valParam = validateParams($allPostParams); //primero valido los parámetros

    $data = [];

    if ($valParam == "OK") {
        $VFPConnObj = new VFPConnector();

        // $MYSQLConnObj = new MySQLConnection();
        $VFPConn = $VFPConnObj->getConnection();

        // $mySQLConn = $MYSQLConnObj->getConnection();
        $observa = $allPostParams['data']['observa'];
        $cve_cte = $allPostParams['data']['cve_cte'];
        $cve_age = $allPostParams['data']['cve_age'];
        // $lista_precData = getListaPorCliente($VFPConn, $cve_cte);
        // $lista_prec = $lista_precData[0]['lista_prec'];

        $subtotalTotal = 0;
        $totalTotal = 0;
        $ivaTotal = 0;
        $firstTime = true;

        //checamos si tiene descuento
        $descue = 0;
        $descuento = 0;
        $descue = isset($allPostParams['data']['descuento']) ? $allPostParams['data']['descuento'] : null;
        if (!($descue && is_numeric($descue))) {
            $descue = 0;
            $descuento = 0;
        } else {
            $descue = floatval($descue);
        }

        foreach ($allPostParams['data']['pedidos'] as $pedido) {
            //sacamos el precio TOTAL de todos los pedidos para insertar en pedidoc antes
            $precio = $pedido['precio'];
            // }
            // pre($precioData);
            // die();
            $cantidad = $pedido['cantidad'];
            $subtotalTotal += $precio * $cantidad;
        }

        $lastId = getNextPedidocId($VFPConn);
        $lastId = intval($lastId);
        $lastId++;

        $lastMovsId = getNextMovsId($VFPConn);
        $lastMovsId = intval($lastMovsId);
        $lastMovsId++;

        foreach ($allPostParams['data']['envios'] as $envio) {
            // $enviosArr = getEnvios($mySQLConn, intval($envio['id']));

            // $precio = floatval($enviosArr[0]['precio']);
            $cantidad = intval($envio['cantidad']);

            $dataD = array();
            $dataD['no_ped'] = intval($lastId);
            $dataD['cve_prod'] = "'".$envio['cve_prod']."'"; // "'".$enviosArr[0]['cve_prod']."'";
            $dataD['cse_prod'] = "'".$envio['cse_prod']."'"; // "'".$enviosArr[0]['cse_prod']."'";
            $dataD['cant_prod'] = intval($cantidad);
            $dataD['valor_prod'] = floatval($envio['precio']);
            $dataD['cve_suc'] = "'MAT'";
            $dataD['new_med'] = "''";
            $dataD['saldo'] = intval($cantidad);
            insertPedidod($VFPConn, $dataD);

            $subtotalTotal += $precio * $cantidad;
        }
        $descuento = floatval($subtotalTotal * $descue / 100);
        // $subtotalTotal = $subtotalTotal - $descuento;
        $ivaTotal = floatval(($subtotalTotal - $descuento) * .16);
        $totalTotal = floatval($subtotalTotal) - $descuento + floatval($ivaTotal);

        foreach ($allPostParams['data']['pedidos'] as $pedido) {
            $precio = $pedido['precio'];
            $descuentopv = isset($pedido['descuento']) ? $pedido['descuento'] : 0;
            $cse_prod = $pedido['cse_prod'];
            $cantidad = $pedido['cantidad'];
            $cve_prod = $pedido['cve_prod'];

            $color = $pedido['new_med'];
            $cto_ent = floatval($pedido['cto_ent']);
            if ($firstTime) {
                //solo se inserta un pedidoc por todos los pedidos
                $data = array();
                $data['no_ped'] = intval($lastId);
                $data['subt_ped'] = floatval($subtotalTotal);
                $data['iva'] = $ivaTotal;
                $data['total_ped'] = $totalTotal;

                $data['observa'] = "'".$observa."'";
                $data['cve_suc'] = "'MAT'";
                $data['usuario'] = $cve_age;
                $data['trans'] = 1;
                $data['lugar'] = "'MTY'";
                $data['cve_cte'] = $cve_cte;
                $data['cve_age'] = $cve_age;
                $data['descuento'] = $descuento;
                $data['descue'] = $descue;
                insertPedidoc($VFPConn, $data);

                $firstTime = false;
            }

            $dataD = array();
            $dataD['no_ped'] = intval($lastId);
            $dataD['cve_prod'] = "'".$cve_prod."'";
            $dataD['cse_prod'] = "'".$cse_prod."'";
            $dataD['cant_prod'] = intval($cantidad);
            $dataD['valor_prod'] = floatval($precio);
            $dataD['cve_suc'] = "'MAT'";
            $dataD['new_med'] = "'".$color."'";
            $dataD['saldo'] = intval($cantidad);
            $dataD['descuento'] = $descuentopv;
            insertPedidod($VFPConn, $dataD);
        }
        $data = [
            'data' => [
                'status' => $code,
                'descripcion' => 'Pedido insertado con exito',
                'noPedido' => $lastId,
            ],
        ];
    } else {
        $code = 500;
        $data = [
            'data' => [
                'error' => [
                    'status' => $code,
                    'descripcion' => $valParam,
                ],
            ],
        ];
    }

    return response()->json($data, $code);
});

Route::post('pedidos/{id}/cancelar', function ($id) {
    $data = [];
    $code = 200;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    try {
        $sql = "update pedidoc set status = 'Cancelado' where no_ped = ".$id;
        $rs = $conn->Execute($sql);
        $sql = "update pedidod set status1 = 'C' where no_ped = ".$id;
        $rs = $conn->Execute($sql);

        return "OK";
    } catch (Exception $e) {
        return "Error:".$e->getMessage();
    }
});
